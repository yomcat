\documentclass[11pt,a4paper]{article}

\usepackage{amssymb, amsthm, enumerate, amsmath, pictexwd, mathrsfs, ../../yomcat, ../../diagrams}

\pagestyle{empty}

\theoremstyle{definition}
\newtheorem{Q}{Question}
\newtheorem*{sol}{Solution}
\newtheorem*{solution}{Solution}

\setlength\arraycolsep{2pt}
\setlength\headheight{0pt}
\setlength\headsep{0pt}
\setlength\textwidth{400pt}
\setlength\marginparwidth{0pt}

\makeatletter
\def\imod#1{\allowbreak\mkern10mu({\operator@font mod}\,\,#1)}
\makeatother

\setlength{\parskip}{1ex}
\setlength{\parindent}{0pt}

\begin{document}

\begin{center}
{\bf Victoria University of Wellington}\\[1ex]
{\bf School of Mathematics, Statistics \& Operations Research}\\
\end{center}

\hrule
MATH 436 \hfill
Assignment 7 \hfill
Michael Welsh (301012788)
\vspace{1ex}\hrule\vspace{1ex}

\qs
\begin{enumerate}
	\item $M$ and $P$ are submodules of $N$. Show that $M \otimes \frac{N}{P} \cong \frac{M \otimes N}{M \otimes P}$.
	\item Show that $K[x] \otimes_K \Omega$ is isomorphic to $\Omega[x]$.
	\item $K = {\rm Frac}(A)$. $L$ is a field extension of $K$. Show that $\mbc{O}_L$ is integrally closed.
	\item $d$ is a square-free integer and $K = \mbb{Q}(\sqrt{d})$. Show that $\mbc{O}_K = \mbb{Z}(1, \sqrt{d})$ if $d \equiv 2, 3$ mod 4; or $\mbc{O}_K = \mbb{Z}(1, \frac{1 + \sqrt{d}}{2})$ if $d \equiv 1$ mod 4.
	\item \textit{(Bonus)} $K = \mbb{Q}(\sqrt[3]{2})$. Find $\mbc{O}_K$
	\item \textit{(Bonus)} $\omega$ is a primitive 5th root of unity and $K = \mbb{Q}(\omega)$. Show that $\mbc{O}_K = \mbb{Z}[\omega]$.
\end{enumerate}

\as
\begin{enumerate}
	\item $f: M \times \frac{N}{P} \to Q$ is a $K$-bilinear map. Construct $\ol{f}(\frac{m \otimes n}{m \otimes p}) \define f(m, \frac{n}{p})$. Then the following diagram commutes
		\begin{diagram}[nohug]
		(m, \frac{n}{p}) & & M \times \frac{N}{P} & \rTo^f & Q \\
		\dMapsto & & \dTo & \ruTo^{\ol{f}} & \\
		\frac{m \otimes n}{m \otimes p} & & \frac{M \otimes N}{M \otimes P} & & 
		\end{diagram}
	And therefore $\frac{M \otimes N}{M \otimes P} \cong M \otimes \frac{N}{P}$ \pend
	\item $h: K[x] \times \Omega \to L$ is a bilinear map. Construct \ol{h} on $\Omega[x]$ as $\ol{h}(\omega f(x)) \define h(f(x), \omega)$. Then the following diagram commutes and therefore $K[x] \otimes \Omega \cong \Omega[x]$. \pend
		\begin{diagram}[nohug]
		(f(x), \omega) & & K[x] \times \Omega & \rTo^h & L \\
		\dMapsto & & \dTo & \ruTo^{\ol{h}} & \\
		\omega f(x) & & \Omega[x] & &
		\end{diagram}
	\item By Propositions 2.6 and 2.7, $L = {\rm Frac}(\mbc{O}_L)$. By definition, $\mbc{O}_L = \{ x \in L | x\ {\rm is\ integral\ over}\ A \}$. If $x$ is integral over $A$, then it is also integral over $K$, as $A \subseteq K$. $L$ is an extension of $K$, so $K \subseteq L$ and $x$ is also integral over $L$. Therefore, $x^n + \frac{a_1}{b_1}x^{n-1} + \cdots + \frac{a_n}{b_n} = 0$, for some $a_i, b_i \in \mbc{O}_L$. Multiply each coefficient by the lowest common multiple of all the $b_i$s, $b$, to get $bx^n + a_1bx^{n-1} + \cdots + ba_n = 0$. Therefore, $bx \in L$ is integral over $\mbc{O}_L$, and therefore $\mbc{O}_L = \{ x \in L | x\ {\rm is\ integral\ over}\ \mbc{O}_L \}$. \pend
	\item Suppose that $a + b\sqrt{d}$ is integral over \mbb{Z}. Then $a + b\sqrt{d}$ is a root of $x^2 - a_1x + a_2 \in \mbb{Z}[x]$, where $a_1, a_2 \in \mbb{Z}$. \begin{align*}
		a_1 &= (a + b\sqrt{d}) + (a - b\sqrt{d}) \\
			&= 2a \\
		\therefore a &= \frac{a_1}{2} \\
		a_2 &= (a + b\sqrt{d})(a - b\sqrt{d}) \\
			&= a^2 - db^2 
		\end{align*}
	Assume $b = \frac{b_1}{2}$ for some $b_1 \in \mbb{Z}$. Then \[ a_2 = \frac{a_1^2 - db_1^2}{4} \]
	Then $a_1^2 - db_1^2 = 4a_2 \equiv 0$ mod 4. \\
	Now, there are three cases. $d \not\equiv 0$ mod 4 as $d$ is square-free.
		\begin{enumerate}
			\item[$d \equiv 1$ mod 4:] In this case, either $a_1 \equiv b_1 \equiv 0$ mod 2 or $a_1 \equiv b_1 \equiv 1$ mod 2. We already know that $a + b\sqrt{d} = \frac{a_1 + b_1\sqrt{d}}{2}$. However, $a_1$ and $b_1$ could be either even or odd, so no cancelling is possible. Hence every element of $\mbc{O}_K$ is a linear combination of 1 and $\frac{1 + \sqrt{d}}{2}$, so $\mbc{O}_K = \mbb{Z}(1, \frac{1 + \sqrt{d}}{2})$.
			\item[$d \equiv 2$ mod 4:] In this case, $a_1 \equiv b_1 \equiv 0$ mod 2. We already know that $a + b\sqrt{d} = \frac{a_1 + b_1\sqrt{d}}{2}$. But both $a_1$ and $b_1$ are even, so $a, b \in \mbb{Z}$ and therefore every element of $\mbc{O}_K$ is a linear combination of $a$ and $b\sqrt{d}$, so $\mbc{O}_K = \mbb{Z}(1, \sqrt{d})$.
			\item[$d \equiv 3$ mod 4:] This case is the same as the previous case ($d \equiv 2$ mod 4), so $\mbc{O}_K = \mbb{Z}(1, \sqrt{d})$.
		\end{enumerate}
	\item
	\item
\end{enumerate}

\end{document}