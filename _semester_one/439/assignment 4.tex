\documentclass[11pt,a4paper]{article}

\usepackage{amssymb, amsthm, enumerate, amsmath, pictexwd, mathrsfs, ../../yomcat, ../../diagrams, ../../dcpic}

\pagestyle{empty}

\theoremstyle{definition}
\newtheorem{Q}{Question}
\newtheorem*{sol}{Solution}
\newtheorem*{solution}{Solution}

\setlength\arraycolsep{2pt}
\setlength\headheight{0pt}
\setlength\headsep{0pt}
\setlength\textwidth{400pt}
\setlength\marginparwidth{0pt}

\makeatletter
\def\imod#1{\allowbreak\mkern10mu({\operator@font mod}\,\,#1)}
\makeatother

\setlength{\parskip}{1ex}
\setlength{\parindent}{0pt}

\begin{document}

\begin{center}
{\bf Victoria University of Wellington}\\[1ex]
{\bf School of Mathematics, Statistics \& Operations Research}\\
\end{center}

\hrule
MATH 439 \hfill
Assignment 4 \hfill
Michael Welsh (301012788)
\vspace{1ex}\hrule\vspace{1ex}

\qs

\begin{enumerate}[(1)]
	\item Recall that a complete semi-lattice is a partial order $Q$ in which every subset $S \subset Q$ has a least upper bound in $Q$. Let \scr{P} be the {\textit covariant power set functor} on \textbf{Set} so that \scr{P}$X$ is the set of all subsets $S \subset X$, while for each function $f : X \to Y$, $(\scr{P}f)S$ is the direct image of $S$ under $f$. For each set $X$, let $\eta_X : X \to \scr{P}X$ send each $x \in X$ to the one point set $\{x\}$, while $\mu_X : \scr{PP}X \to \scr{P}X$ sends each set of sets into its union. \begin{enumerate}[(a)]
		\item Prove that \tuple{\scr{P}, \eta, \mu} is a monad \scr{P} on \textbf{Set}
		\item Prove that each \scr{P}-algebra \tuple{X, h} is a complete semi-lattice when $x \leqq y$ is defined by $h\{x, y\} = y$, and sup$S = hS$ for each $S \subset X$.
		\item Prove conversely that every (small) complete semi-lattice is a \scr{P}-algebra in this way.
		\item Conclude that the category of \scr{P}-algebras is the category of all (small) complete semi-lattices, with morphisms the order and sup-preserving functions.
	\end{enumerate}
	\item \begin{enumerate}[(a)]
		\item For monads \tuple{T, \eta, \mu} and \tuple{T', \eta', \mu'} on $X$, define a morphism $\theta$ of monads as a suitable natural transformation $\theta : T \rTo^{\bullet} T'$. and construct the category of all monads in $X$.
		\item From $\theta$ construct a functor $\theta^* : X^{T'} \rTo X^T$ such that $G^T \circ \theta^* = G^{T'}$ and a natural transformation $F^T \rTo^\bullet \theta^* \circ F^{T'}$.
	\end{enumerate}
	\item Let $W_0$ be the monad in \textbf{Set} defined by the forgetful functor \textbf{Mon} $\to$ \textbf{Set}. Show that a $W_0$-algebra is a set $M$ with a string $v_0, v_1, \dots$ of $n$-ary operations $v_n$, where $v_0 : * \to M$ is the unit of the monoid $M$ and $v_n$ is the $n$-fold product.
	\item For any ring $R$ with identity, the forgetful functor $G : R-\textbf{Mod} \to \textbf{Set}$ from the category of left $R$-modules has a left adjoint and so defines a monad \tuple{T_R, \eta, \mu} in \textbf{Set}. \begin{enumerate}[(a)]
		\item Prove that this monad may be described as follows: For each set $X$, $T_RX$ is the set of all those functions $f : X \to R$ with only a finite number of non-zero values; for each function $t : X \to Y$ and each $y \in Y$, $|(T_Rt)f|_y = \Sigma f_x$ with sum taken over all $x \in X$ with $tx = y$; for each $x \in X$, $\eta_xx : X \to R$ is defined by$(\eta_xx)x = 1$, $(\eta_xx)x' = 0$; for each $k \in T_R(T_RX)$, $\mu_xk : X \to R$ is defined for $x \in X$ by $(\mu_xk)_x = \Sigma_fk_ff_x$, the sum taken over all $f \in T_RX$.
		\item From this description, verify directly that \tuple{T_R, \eta, \mu} is a monad.
		\item Show that the \tuple{T_R, \eta, \mu}-algebras are the usual $R$-modules, described not via addition and scalar multiple, but via all operations of linear combination (The structure map $h$ assigns to each $f$ the ``linear combination with coefficients $f_x$ for each $x \in X$''.)
	\end{enumerate}
	\item Construct the Kleisli comparison functor $L$, prove its uniqueness, and show that the image of $X_T$ under $L$ is the full subcategory $FX$ of $A$ with objects all $Fx$, $x \in X$.
	\item Show that the restriction of $L$ gives an equivalence of categories $X_T \to FX$.
	\item Construct an example of an adjunction where $F$ is not a bijection on objects. Deduce that the equivalence $X_T \to FX$ in Question 6 need not be an isomorphism. (Suggestion: $S \mapsto T(S)$ = the one-point-set defines a monad in \textbf{Set}.)
	\item If $\tuple{F, G, \eta, \varepsilon}: X\to B$ defines the monad \tuple{T, \eta, \mu} in $X$, while a second adjunction  $\tuple{L, R, \eta', \varepsilon'}: B \to A$ defines the identity monad in $B$ (i.e., $RL = I_B$, $\eta' = 1$, and $R\varepsilon'L = 1$), prove that the composite adjunction $X \to A$ defines in $X$ the same monad \tuple{T, \eta, \mu}.
\end{enumerate}

\as

\begin{enumerate}[(1)]
	\item \begin{enumerate}[(a)]
		\item If \tuple{\scr{P}, \eta, \mu} is a monad, then the following diagrams will commute:
		\begin{diagram}
			\scr{P}^3(X) & \rTo^{\scr{P}\mu} & \scr{P}^2(X)	& &	\scr{P}(X) & \rTo^{\eta\scr{P}} & \scr{P}^2(X) & \lTo^{\scr{P}\eta} & \scr{P}(X) \\
			\dTo^{\mu\scr{P}} & & \dTo_\mu					& (1) &	& & \dTo_\mu & & (2)  \\
			\scr{P}^2(X) & \rTo_\mu & \scr{P}(X)				& &	& & \scr{P}(X) & &
		\end{diagram}
		For diagram (1), note that $\scr{P}\mu(x) = \cup x = \mu\scr{P}(x)$, so the two $\scr{P}^2(X)$ vertices have the same elements, and the diagram commutes. \\
		For diagram (2), note that $\cup \{x\} = x$, and so this diagram also commutes.
		As both diagrams commute, \tuple{\scr{P}, \eta, \mu} is a monad. \pend
		\item The arrow $h$ is defined from $\scr{P}X \to X$, and $S \subset X$, so $S \in \scr{P}X$. Therefore $hS$ is defined for all $S$, and so each $S$ has a least upper bound in $\scr{P}X$ and therefore each \scr{P}-algebra is a complete semi-lattice. \pend
		\item Let $Q$ be a complete semi-lattice and let $S \subset Q$ have a least upper bound in $Q$, $b$. Then $\forall q \in Q$, $q \leqq b$. This fits with the description of a \scr{P}-algebra given, as sup$S = hS$ and $h\{x,y\} = y$ where $x \leqq y$. So every (small) complete semi-lattice is a \scr{P}-algebra in this way. \pend
		\item These two facts show that the if the category of all (small) complete semi-lattices has arrows which preserve order and sup (as the operation of $h$ needs to be preserved) then it is identical to the category of \scr{P}-algebras, as each \scr{P}-algebra is equivalent to a (small) complete semi-lattice.
	\end{enumerate}
	\item \begin{enumerate}[(a)]
		\item A morphism $\theta$ of monads \tuple{T, \eta, \mu} and \tuple{T', \eta', \mu'} is a natural transformation $\theta: T \to T'$ such that the following diagrams commute
			\begin{diagram}
				T^2 & \rTo^{T\theta} & T'^2 & & \id{X} & \rLine & \id{X} \\
				\dTo^\mu & & \dTo_{\mu'} & & \dTo^\eta & & \dTo_{\eta'} \\
				T & \rTo_{\theta} & T'& & T & \rTo_\theta & T'
			\end{diagram}
		So the category \cat{Mnd$_X$} has as objects all the monads in $X$ with arrows the morphisms between monads, as defined above.
		\item $\theta^*$ is a functor such that the following diagram commutes for $G^T: X^T \to Y$
			\begin{diagram}[nohug]
				X^{T'} & \rTo^{\theta^*} & X^T \\
				& \rdTo_{G^{T'}} & \dTo_{G^T} \\
				& & Y
			\end{diagram}
	\end{enumerate}
	\item Applying the Corollary to \cat{Mon}: \\
		The system \tuple{M, v_o, v_1, \dots} is a $W_0$-algebra iff $v_0 = e$, $v_1: M \to M$ is an associative unary operation on $M$, and for all $n \geq 1$, $v_{n+1} = v_n(v_1 \times e): M^{n+1} \to M$. \\
		The given description, with $n$-fold product, fits the conditions, and so the system is a $W_0$-algebra. \pend
	\item \begin{enumerate}[(a)]
		\item See below.
		\item Consider the following diagram
			\[\begindc{\commdiag}[3]
			\obj(10,30)[a]{$T_R^3X$}
			%\obj(3,35)[g]{$a$}
			\obj(30,30)[c]{$T_R^2X$}
			%\obj(37,35)[hg]{$f(a)$}
			\obj(10,10)[b]{$T_R^2X$}
			%\obj(3,5)[gf]{$\{a\}$}
			\obj(30,10)[d]{$T_RX$}
			%\obj(37,5)[hgf]{$f(\{a\})$}
			\mor{a}{b}{$\mu_{T_X}$}[-1,0]
			\mor{c}{d}{$\mu_X$}
			\mor{a}{c}{$T(\mu_X)$}
			\mor{b}{d}{$\mu_X$}[-1,0]
			%\cmor((7,5)(20,4)(31,5)) \pright(0,20){} % bottom
			%\cmor((3,33)(2,20)(3,8)) \pdown(10,10){} %left
			%\cmor((5,35)(20,36)(32,35)) \pright(15,15){} %top
			%\cmor((37,32)(38,20)(37,8)) \pdown(3,4){} %right
			\enddc\]
		The only operation used in this square is addition, and the same elements are being summed in each direction (though in a different order). As addition is commutative, this makes the square commutative. \\
		Now consider the following diagram
			\begin{diagram}
			T_RX & \rTo^{\eta T_RX} & T_R^2X & \lTo^{T_R\eta X} & T_RX \\
			 & & \dTo_{\mu X} & & \\
			 & & T_RX
			\end{diagram}
			Let $x \in T_RX$. Then $\eta T_RX(x) = T_R\eta X(x) = 0$. Furthermore, $\mu X(0) = x$, and so the diagram commutes. \\
		As both diagrams commute, \tuple{T_R, \eta, \mu} is a monad. \pend
		\item The actions of taking scalar multiples and doing addition is just summing all the linear combinations of a certain variable with certain coefficients. $h$ assigns to each $f$ the linear combination with coefficients $f_x$ for each $x \in X$, which is just adding and taking scalar multiples over $R$, defining the $R$-modules.
	\end{enumerate}
	\item
	\item
	\item
	\item The composite of the adjunctions \tuple{F,G,\eta,\varepsilon} and \tuple{L,R,\eta',\varepsilon'} is the adjunction \tuple{LF,GR,G \eta' F \cdot \eta, \varepsilon' \cdot L \varepsilon R}. The monad this composite adjunction defines is \tuple{GRLF, G \eta' F \cdot \eta, GR \varepsilon' \cdot L \varepsilon RLF}. First, $GRLF = GI_BF = GF = T$, so the functor works as desired. Next, $G \eta' F \cdot \eta = G 1 F \cdot \eta = GF \cdot \eta = \eta$, so the unit works as desired. Finally, $GR \varepsilon' \cdot L \varepsilon RLF = GR \varepsilon' L \varepsilon I_B F = G1\varepsilon F = \varepsilon$, so the multiplication works as desired. Therefore the monad \tuple{GRLF, G \eta' F \cdot \eta, GR \varepsilon' \cdot L \varepsilon RLF} is equivalent to the monad \tuple{T, \eta, \varepsilon}, which is the monad defined by the adjunction \tuple{F,G,\eta,\varepsilon}. \pend
\end{enumerate}

\end{document}